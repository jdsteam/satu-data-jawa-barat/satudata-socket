require('dotenv').config()
const fs = require('fs')

const express = require('express')
const app = express()
const port = process.env.PORT || 5004

let httpServer;
const environment = process.env.ENVIRONMENT === 'localhost'

if (environment) {
  httpServer = require('http').createServer(app)
} else {
  httpServer = require('https').createServer({
    key: fs.readFileSync('certs/privkey.pem'),
    cert: fs.readFileSync('certs/fullchain.pem'),
    ciphers: "DEFAULT:!SSLv2:!RC4:!EXPORT:!LOW:!MEDIUM:!SHA1"
  }, app)
}

const io = require('socket.io')(httpServer)

const jwt = require('jsonwebtoken')
const cors = require('cors')
const bodyParser = require('body-parser')
const middleware = require('./middleware')

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.io = io

app.get('/', function (req, res) {
  res.status(200).json({
    error: 0,
    message: 'Welcome to Satu Data Socket',
    data: {}
  })
})

app.get('/notification', [middleware.checkToken, middleware.checkAuth], (req, res) => {
  const receiver = Number(req.query.receiver) || 0
  const roomClient = 'room-' + receiver
  req.app.io.to(roomClient).emit(roomClient, 'triggered')

  res.status(200).json({
    error: 0,
    message: 'Notification sent.',
    data: {}
  })
})

io.on('connection', (socket) => {
  const token = socket.handshake.auth.token

  var options = {
    secret: process.env.JWT_SECRET_KEY,
    timeout: 5000
  }

  var auth_timeout = setTimeout(() => {
    socket.disconnect('unauthorized')
  }, options.timeout || 5000)

  clearTimeout(auth_timeout)

  jwt.verify(token, options.secret, options, (err, decoded) => {
    if (err){
      socket.disconnect('unauthorized');
    }

    if (!err && decoded){
      socket.decoded_token = decoded
      socket.connectedAt = new Date()

      // Disconnect listener
      socket.on('disconnect', () => {
        console.info('SOCKET [%s] DISCONNECTED', socket.id)
      });

      // Rooms
      const roomClient = 'room-' + socket.decoded_token.id
      socket.join(roomClient)

      console.info('SOCKET [%s] CONNECTED', socket.id)
      io.to(roomClient).emit(roomClient, 'authenticated')
    }
  })
});

httpServer.listen(port, () => {
  console.log('Server started on port ' + port)
})
