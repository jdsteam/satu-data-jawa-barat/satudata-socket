const config = require('./config.js')
const jwt = require('jsonwebtoken')
const _ = require('lodash')
const axios = require('axios')

const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

const checkToken = (req, res, next) => {
  if (req.query.from === 'opendata') {
    next()
  }

  if (_.isNil(req.headers.authorization)) {
    res.status(403).json({
      error: 1,
      message: 'Token Missing',
      data: {}
    })
  }

  let token = req.headers.authorization
  if (token.startsWith('Bearer ')) {
    token = token.slice(7, token.length)
  }

  if (token) {
    jwt.verify(token, config.secret, (err, decoded) => {
      if (err) {
        return res.status(403).json({
          success: false,
          message: 'Token invalid'
        })
      } else {
        req.decoded = decoded
        next()
      }
    })
  } else {
    res.status(403).json({
      error: 1,
      message: 'Auth token is not supplied',
      data: {}
    })
  }
}

const checkAuth = async (req, res, next) => {
  if (req.query.from === 'opendata') {
    next()
  }
  
  if (_.isNil(req.headers.authorization)) {
    res.status(403).json({
      error: 1,
      message: 'Token Missing',
      data: {}
    })
  }

  const token = req.headers.authorization
  const appCode = process.env.APP_CODE
  const controller = req.route.path.substr(1)
  const action = req.method

  const fullurl = process.env.ROLECHECK_URL + 'auth/check?app_code=' + appCode + '&controller=' + controller + '&action=' + action

  try {
    axios.get(fullurl, { headers: { Authorization: token } })
      .then(response => {
        next()
      })
      .catch(() => {
        res.status(403).json({
          error: 1,
          message: 'App Code, Controller, or Action not found',
          data: {}
        })
      })
  } catch (e) {
    console.log('Connection refused by the server..')
    console.log('Let me sleep for 5 seconds')
    console.log('ZZzzzz...')
    await sleep(5000)
    console.log('Was a nice sleep, now let me continue...')
  }
}

module.exports = {
  checkToken: checkToken,
  checkAuth: checkAuth
}
